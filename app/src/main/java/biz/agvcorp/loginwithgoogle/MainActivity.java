package biz.agvcorp.loginwithgoogle;

import android.app.Dialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GooglePlayServicesAvailabilityIOException;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.people.v1.PeopleService;
import com.google.api.services.people.v1.model.Person;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import net.danlew.android.joda.JodaTimeAndroid;

import org.joda.time.LocalDate;
import org.joda.time.Years;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

//import android.net.wifi.hotspot2.pps.Credential;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {

    ProgressDialog mProgress;

    private LinearLayout activity_main;
    private ImageView photoImageView;
    private TextView nameTextView;
    private TextView emailTextView;
    private TextView idTextView;
    private TextView addressTextView;
    private TextView birthdayTextView;
    private TextView ageTextView;
    private TextView genderTextView;
    private TextView localsTextView;
    private TextView occupationTextView;
    private TextView relationshipstausTextView;
    private TextView phoneTextView;

    final int RC_API_CHECK = 100;
    static final int REQUEST_AUTHORIZATION = 1001;
    static final int REQUEST_GOOGLE_PLAY_SERVICES = 1002;

    private GoogleApiClient googleApiClient;
    private GoogleSignInOptions gso;

    private static HttpTransport HTTP_TRANSPORT = AndroidHttp.newCompatibleTransport();
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener firebaseAuthListener;

    public static boolean isAppRunning;

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        isAppRunning = true;

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        String channelId = "1";
        String channel2 = "2";

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(channelId, "Channel 1",NotificationManager.IMPORTANCE_HIGH);

            notificationChannel.setDescription("This is BNT");
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setShowBadge(true);
            notificationManager.createNotificationChannel(notificationChannel);

            NotificationChannel notificationChannel2 = new NotificationChannel(channel2, "Channel 2",NotificationManager.IMPORTANCE_MIN);

            notificationChannel.setDescription("This is bTV");
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setShowBadge(true);
            notificationManager.createNotificationChannel(notificationChannel2);

        }

        sharedPreferences = getBaseContext().getSharedPreferences(getString(R.string.FCM_PREF), Context.MODE_PRIVATE);

        /*Joda Library initialization*/
        JodaTimeAndroid.init(this);

        mProgress = new ProgressDialog(this);
        showProgress( getString(R.string.appinintMsg) );

        activity_main = findViewById(R.id.activity_main);
        photoImageView = findViewById(R.id.photoImageView);
        nameTextView = findViewById(R.id.nameTextView);
        emailTextView = findViewById(R.id.emailTextView);
        idTextView = findViewById(R.id.idTextView);
        addressTextView = findViewById(R.id.addressTextView);
        birthdayTextView = findViewById(R.id.birthdayTextView);
        ageTextView = findViewById(R.id.ageTextView);
        genderTextView = findViewById(R.id.genderTextView);
        localsTextView = findViewById(R.id.localsTextView);
        occupationTextView = findViewById(R.id.occupationTextView);
        relationshipstausTextView = findViewById(R.id.relationshipstausTextView);
        phoneTextView = findViewById(R.id.phoneTextView);

        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestServerAuthCode(getString(R.string.clientID))
                .requestEmail()
                .requestScopes(new Scope(Scopes.PROFILE))
                .build();

        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addOnConnectionFailedListener(this)
                .addConnectionCallbacks(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null){
                    setuserDate_firebase(user);
                } else {
                    //logoutonlyfromGoogle();
                    goLogInScreen();
                }
            }
        };

        //sendFCMTokenToServerPHP(getFcmToken());
    }

    private void sendFCMTokenToServerPHP(String fcmToken) {
        String url_fcmToken = getString(R.string.host_url)+"/"+getString(R.string.fcmTokenuri);

        Map<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("fcmToken",fcmToken);

        JsonObjectRequest tokenRequest = new JsonObjectRequest(Request.Method.POST, url_fcmToken, new JSONObject(jsonParams), new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                hideProgress();
                try {
                    if (response.getBoolean("status")){
                        getSnackbar(response.getString("message"), Snackbar.LENGTH_SHORT).show();
                    } else {
                        getSnackbar(response.getString("message"), Snackbar.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    getSnackbar(String.valueOf(e), Snackbar.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgress();
            }
        });
        Singleton.getmInstance(MainActivity.this).addtoRequestQueue(tokenRequest);
    }

    @Override
    protected void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(firebaseAuthListener);

        googleApiClient.connect();
        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(googleApiClient);
        if (opr.isDone()){
            GoogleSignInResult result = opr.get();
            handleSignInResult(result);
        } else {
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(@NonNull GoogleSignInResult googleSignInResult) {
                    handleSignInResult(googleSignInResult);
                }
            });
        }
    }

    private String profilepicurl;
    private String displayname;
    private String googleid;
    private String useremail;

    public void setuserDate_firebase(FirebaseUser user) {
        displayname = user.getDisplayName();
        googleid = user.getUid();
        profilepicurl = String.valueOf(user.getPhotoUrl());
        useremail = user.getEmail();

        nameTextView.setText( user.getDisplayName() );
        emailTextView.setText("Email Address - "+user.getEmail() );
        idTextView.setText("Google ID - "+user.getUid() );
        Glide.with(this).load( user.getPhotoUrl() ).into(photoImageView);
    }

    private void handleSignInResult(GoogleSignInResult result) {
        hideProgress();
        if (result.isSuccess()) {
            GoogleSignInAccount account = result.getSignInAccount();
            new PeopleAsync().execute(account.getServerAuthCode());
        } else {
            getSnackbar(getString(R.string.googlesignInErrMsg), Snackbar.LENGTH_LONG).show();
        }
    }

    private void goLogInScreen() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public void logOut(View view) {
        firebaseAuth.getInstance().signOut();
        Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {
                if (status.isSuccess()) {
                    goLogInScreen();
                } else {
                    getSnackbar(getString(R.string.not_close_session), Snackbar.LENGTH_SHORT).show();
                }
            }
        });
    }

    private boolean isSignedIn() {
        return GoogleSignIn.getLastSignedInAccount(this) != null;
    }

    private void logoutonlyfromGoogle(){
        if (isSignedIn()){
            Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(new ResultCallback<Status>() {
                @Override
                public void onResult(@NonNull Status status) {
                    if (status.isSuccess()) {
                        goLogInScreen();
                    } else {
                        getSnackbar(getString(R.string.not_close_session), Snackbar.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    public void revoke(View view) {
        firebaseAuth.getInstance().signOut();
        Auth.GoogleSignInApi.revokeAccess(googleApiClient).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {
                if (status.isSuccess()) {
                    goLogInScreen();
                } else {
                    getSnackbar(getString(R.string.not_revoke), Snackbar.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d("connection", "msg: " + connectionResult.getErrorMessage());
        GoogleApiAvailability mGoogleApiAvailability = GoogleApiAvailability.getInstance();
        Dialog dialog = mGoogleApiAvailability.getErrorDialog(this, connectionResult.getErrorCode(), RC_API_CHECK);
        dialog.show();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    private Snackbar getSnackbar(String message, int durationCode){
        return Snackbar.make(activity_main, message, durationCode);
    }

    /**
     * Display an error dialog showing that Google Play Services is missing
     * or out of date.
     * @param connectionStatusCode code describing the presence (or lack of)
     *     Google Play Services on this device.
     */
    void showGooglePlayServicesAvailabilityErrorDialog(
            final int connectionStatusCode) { GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        Dialog dialog = apiAvailability.getErrorDialog(
                MainActivity.this,
                connectionStatusCode,
                REQUEST_GOOGLE_PLAY_SERVICES);
        dialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_GOOGLE_PLAY_SERVICES:
                if (resultCode != RESULT_OK){

                } else {

                }
        }
    }

    private void showProgress(String msg){
        mProgress.setMessage(msg);
        mProgress.show();
    }
    private void hideProgress(){
        mProgress.setMessage("");
        mProgress.hide();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mProgress.dismiss();
        if (firebaseAuthListener != null){
            firebaseAuth.removeAuthStateListener(firebaseAuthListener);
        }
    }

    private String getFcmToken(){
        return sharedPreferences.getString(getString(R.string.FCM_TOKEN), "");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isAppRunning = false;
    }












    private class PeopleAsync extends AsyncTask<String, Void, Person>{

        private String birthdayStr, addressStr, genderStr, localeStr, occupationStr, relationStatusStr, phoneNumberStr, agestr;
        private Date birthdaydate;
        private  Exception mLastError = null;

        private Years getAge(String date){
            DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MM-yyyy");
            LocalDate bDate = formatter.parseLocalDate(date);
            Years age = Years.yearsBetween(bDate, LocalDate.now());
            Log.d("Age", String.valueOf(age));
            return age;
        }

        private Date ConvertToDate(String dateString){
            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
            Date convertedDate = new Date();
            try {
                convertedDate = format.parse(dateString);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return convertedDate;
        }

        private String parseJsontoString(Person person, String param, String params){
            JSONArray temp;
            String res = "";
            try {
                switch (params){
                    case "gender":
                        temp = new JSONArray(person.getGenders().toString());
                        res = temp.getJSONObject(0).get(param).toString();
                        break;
                    case "address":
                        temp = new JSONArray(person.getAddresses().toString());
                        res = temp.getJSONObject(0).get(param).toString();
                        break;
                    case "locale":
                        temp = new JSONArray(person.getLocales().toString());
                        res = temp.getJSONObject(0).get(param).toString();
                        break;
                    case "occupation":
                        temp = new JSONArray(person.getOccupations().toString());
                        res = temp.getJSONObject(0).get(param).toString();
                        break;
                    case "relationStatus":
                        temp = new JSONArray(person.getRelationshipStatuses().toString());
                        res = temp.getJSONObject(0).get(param).toString();
                        break;
                    case "phonenumber":
                        temp = new JSONArray(person.getPhoneNumbers().toString());
                        res = temp.getJSONObject(0).get(param).toString();
                        break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return res;
        }

        private void sendtoPHPServer(){
            String uri = getString(R.string.host_url)+"/"+getString(R.string.registration_addr);

            RequestQueue queue = Volley.newRequestQueue(MainActivity.this);

            Map<String, String> jsonParams = new HashMap<String, String>();
            jsonParams.put("profilePicUrl",profilepicurl);
            jsonParams.put("displayName", displayname);
            jsonParams.put("emailAddress",useremail);
            jsonParams.put("googleId", googleid);
            jsonParams.put("phoneNumber",phoneNumberStr);
            jsonParams.put("birthday", birthdayStr);
            jsonParams.put("age", agestr);
            jsonParams.put("locale", localeStr);
            jsonParams.put("occupation", occupationStr);
            jsonParams.put("gender", genderStr);
            jsonParams.put("address", addressStr);
            jsonParams.put("fcmToken",getFcmToken());

            Log.d("jsonParams", String.valueOf(jsonParams));

            JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, uri, new JSONObject(jsonParams), new Response.Listener<JSONObject>() {

                        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                        @Override
                        public void onResponse(JSONObject response) {
                            hideProgress();
                            try {
                                if (response.getBoolean("status")){
                                    getSnackbar(getString(R.string.phpserversuccessMsg), Snackbar.LENGTH_SHORT).show();
                                } else {
                                    getSnackbar(getString(R.string.phpservererrorMsg), Snackbar.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                getSnackbar(String.valueOf(e), Snackbar.LENGTH_SHORT).show();
                            }
                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            hideProgress();
                        }
                    });
            queue.add(postRequest);
        }

        @Override
        protected Person doInBackground(String... params) {
            Person profile = new Person();

            try {
                PeopleService peopleService = PeopleHelper.setUp(MainActivity.this, params[0]);
                profile = peopleService.people().get("people/me")
                        .setPersonFields("names,emailAddresses,ageRanges,addresses,birthdays,phoneNumbers,genders,locales,occupations,relationshipStatuses")
                        .execute();

                if (profile == null){

                } else {
                    Log.d("RESULT", String.valueOf(profile));

                    if (profile.getAddresses() != null){
                        addressStr = parseJsontoString(profile, "formattedValue", "address");
                    } else {
                        addressStr = getString(R.string.noaddressText);
                    }

                    if (profile.getBirthdays() != null){
                        try {
                            JSONArray birthday = new JSONArray(profile.getBirthdays().toString());
                            JSONObject date = (JSONObject) birthday.getJSONObject(0).get("date");
                            String dateStr = date.get("day").toString()+"-"+date.get("month").toString()+"-"+date.get("year").toString();

                            birthdayStr = dateStr;
                            birthdaydate = ConvertToDate(dateStr);

                            agestr = String.valueOf(getAge(birthdayStr).getYears());
                        } catch (JSONException e) {
                            e.printStackTrace();
                            birthdayStr = getString(R.string.noBirthdayText);
                            agestr = getString(R.string.noageText);
                        }
                    } else {
                        birthdayStr = getString(R.string.noBirthdayText);
                        agestr = getString(R.string.noageText);
                    }

                    if (profile.getGenders() != null){
                        genderStr = parseJsontoString(profile, "formattedValue", "gender");
                    } else {
                        genderStr = getString(R.string.noGenderText);
                    }

                    if (profile.getLocales() != null){
                        localeStr = parseJsontoString(profile, "value", "locale");
                        Log.d("Locals", localeStr);
                    } else {
                        localeStr = getString(R.string.noLocatText);
                    }

                    if (profile.getOccupations() != null){
                        occupationStr = parseJsontoString(profile, "value", "occupation");
                        Log.d("Locals", occupationStr);
                    } else {
                        occupationStr = getString(R.string.nooccupationText);
                    }

                    if (profile.getRelationshipStatuses() != null){
                        relationStatusStr = parseJsontoString(profile, "formattedValue", "relationStatus");
                    } else {
                        relationStatusStr = getString(R.string.relationStsText);
                    }

                    if (profile.getPhoneNumbers() != null){
                        phoneNumberStr = parseJsontoString(profile, "value", "phonenumber");
                    } else {
                        phoneNumberStr = getString(R.string.noPhoneText);
                    }
                }

            } catch (IOException e) {
                mLastError = e;
                cancel(true);
                e.printStackTrace();
                return null;
            }
            return profile;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress( getString(R.string.fetchProfileDataMsg) );
        }

        @Override
        protected void onPostExecute(Person profile) {
            super.onPostExecute(profile);
            hideProgress();

            addressTextView.setText("Address - "+addressStr);
            birthdayTextView.setText("Birthday - "+birthdayStr);
            ageTextView.setText("Age - "+agestr+" years");
            genderTextView.setText("Gender - "+genderStr);
            localsTextView.setText("Locality code - "+localeStr);
            occupationTextView.setText("Occupation - "+occupationStr);
            relationshipstausTextView.setText("Status - "+relationStatusStr);
            phoneTextView.setText("Phone no - "+phoneNumberStr);

            showProgress( getString(R.string.phpsendinginitMsg) );
            sendtoPHPServer();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onCancelled() {
            hideProgress();
            if (mLastError != null) {
                if (mLastError instanceof GooglePlayServicesAvailabilityIOException) {
                    showGooglePlayServicesAvailabilityErrorDialog(
                            ((GooglePlayServicesAvailabilityIOException) mLastError)
                                    .getConnectionStatusCode());
                } else if (mLastError instanceof UserRecoverableAuthIOException) {
                    startActivityForResult(
                            ((UserRecoverableAuthIOException) mLastError).getIntent(),
                            MainActivity.REQUEST_AUTHORIZATION);
                } else {
                    getSnackbar("The following error occurred:\n" + mLastError.getMessage(), Snackbar.LENGTH_INDEFINITE).show();
                }
            } else {
                getSnackbar("Request cancelled.", Snackbar.LENGTH_SHORT).show();
            }
        }
    }
}


//PHP API PROFILE DATE parameters
/*profilePicUrl
displayName
emailAddress
googleId
phoneNumber
address
birthday
age
gender
locale
occupation*/
