package biz.agvcorp.loginwithgoogle.notifications;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;

import biz.agvcorp.loginwithgoogle.MainActivity;
import biz.agvcorp.loginwithgoogle.R;

public class FcmMessagingService extends FirebaseMessagingService{
    private NotificationManager notificationManager;

    private String msgId, msgTitle, msgBody, imageUrl, priority;
    private int notificationId = 0;

    private static final String NOTIFICATION_ID_EXTRA = "notificationId";
    private static final String IMAGE_URL_EXTRA = "imageUrl";
    private static final String ADMIN_CHANNEL_ID ="admin_channel";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        notificationId = new Random().nextInt(60000);

        if (remoteMessage.getData().size() > 0){
            msgId = remoteMessage.getMessageId();
            msgTitle = remoteMessage.getData().get("title");
            msgBody = remoteMessage.getData().get("message");
            imageUrl = remoteMessage.getData().get("imageUrl");
            priority = remoteMessage.getData().get("priority");
        }

        if (remoteMessage.getNotification() != null){
            msgId = remoteMessage.getMessageId();
            msgTitle = remoteMessage.getNotification().getTitle();
            msgBody = remoteMessage.getNotification().getBody();
        }


        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        showNotification(remoteMessage);

        super.onMessageReceived(remoteMessage);
    }

    private void showNotification(RemoteMessage remoteMessage) {
        Intent notificationIntent;
        if (MainActivity.isAppRunning){
            notificationIntent = new Intent(this, MainActivity.class);
        } else {
            notificationIntent = new Intent(this, MainActivity.class);
        }

        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        final PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_ONE_SHOT);

        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Bitmap bitmap = getBitmapfromUrl(imageUrl);

        Intent likeIntent = new Intent(this,LikeService.class);
        likeIntent.putExtra(NOTIFICATION_ID_EXTRA,notificationId);
        likeIntent.putExtra(IMAGE_URL_EXTRA,imageUrl);
        PendingIntent likePendingIntent = PendingIntent.getService(this, notificationId+1,likeIntent,PendingIntent.FLAG_ONE_SHOT);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            setupChannels();
        }


        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId);
        if (imageUrl == null){
            notificationBuilder.setSmallIcon(R.mipmap.ic_launcher_round);
            notificationBuilder.setContentTitle(msgTitle);
            notificationBuilder.setContentText(msgBody);
            notificationBuilder.setAutoCancel(true);
            notificationBuilder.setSound(defaultSoundUri);
            notificationBuilder.setContentIntent(pendingIntent);
            notificationBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(msgBody) );
        } else {
            notificationBuilder.setLargeIcon(bitmap);
            notificationBuilder.setSmallIcon(R.mipmap.ic_launcher_round);
            notificationBuilder.setContentTitle(msgTitle);
            notificationBuilder.setStyle(new NotificationCompat.BigPictureStyle()
                            .setSummaryText(msgBody)
                            .bigPicture(bitmap)
                    );
            /*notificationBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(msgBody));
            notificationBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(msgTitle));*/
            notificationBuilder.setContentText(msgBody);
            notificationBuilder.setAutoCancel(true);
            notificationBuilder.setSound(defaultSoundUri);
            notificationBuilder.addAction(R.drawable.ic_favorite, getString(R.string.notification_add_to_cart_button),likePendingIntent);
            notificationBuilder.setContentIntent(pendingIntent);
        }

        Log.d("priority", String.valueOf(priority));

        if (priority != null){
            if (priority.equals("high")){
                notificationBuilder.setPriority(NotificationCompat.PRIORITY_HIGH);
            } else if (priority.equals("normal")){
                notificationBuilder.setPriority(NotificationCompat.PRIORITY_LOW);
            }
        } else {
            notificationBuilder.setPriority(NotificationCompat.PRIORITY_LOW);
        }
        notificationBuilder.setDefaults(Notification.DEFAULT_VIBRATE);
        notificationManager.notify(notificationId, notificationBuilder.build());
//        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
    }

    public Bitmap getBitmapfromUrl(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            return BitmapFactory.decodeStream(input);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void setupChannels(){
        CharSequence adminChannelName = getString(R.string.notifications_admin_channel_name);
        String adminChannelDescription = getString(R.string.notifications_admin_channel_description);

        NotificationChannel adminChannel;
        adminChannel = new NotificationChannel(ADMIN_CHANNEL_ID, adminChannelName, NotificationManager.IMPORTANCE_LOW);
        adminChannel.setDescription(adminChannelDescription);
        adminChannel.enableLights(true);
        adminChannel.setLightColor(Color.RED);
        adminChannel.enableVibration(true);
        if (notificationManager != null) {
            notificationManager.createNotificationChannel(adminChannel);
        }
    }
}